# Add yourself some shortcuts to projects you often work on
# Example:
#
# brainstormr=/Users/robbyrussell/Projects/development/planetargon/brainstormr

df=~/.dotfiles
vim=~/.dotfiles/vim
desk=~/Desktop
danro=~/Sites/Danro
github=~/Sites/GitHub
bkwld=~/Sites/BKWLD
webflow=~/Sites/GitHub/webflow/webflow
sublime=~/Library/Application\ Support/Sublime\ Text\ 3/Packages
tram=~/Sites/GitHub/javascript/tram
mash=~/Sites/GitHub/javascript/mash
